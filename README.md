# PHP Client for Middleware NetID Web Services

## Obtaining the Client

### Composer

The recommended way to obtain this client is with [Composer](http://www.getcomposer.com). The following minimal `composer.json` file will pull the client into your project:

```json
{
	"repositories": [
		{
			"type": "vcs",
			"url": "git@git.doit.wisc.edu:adi-ia/rpc-netid-php.git"
		}
	],
	"require": {
		"adi-ia/rpc-netid-php": "1.*"
	}
}
```

Your public SSH key will need to be linked to your git.doit.wisc.edu account. Learn more in the composer documentation about [the composer.json Schema - Repositories](https://getcomposer.org/doc/04-schema.md#repositories)

### Git

Users can clone this repository. The client library files are located in `src/main/edu/wisc/doit`. 

## Getting Started

### Loading the Library

If you are using Composer, you simply need to include `./vendor/autoload.php` once in your application. Any references to the client classes will be automatically imported at runtime.

Otherwise, you will need to include specific classes in the `src/main/edu/wisc/doit` directory of the git repository.

### Client Authentication

The NetID web services is protected by IP restriction and client certificate authentication. [Requesting a client certificate](http://rpctest.services.wisc.edu/developers/cert_request.html)

You will need the client authentication certificate issued to you by Middleware and the corresponding private key. 
These should be combined, unencrypted, into a single PEM file. This client provides a helper function to do this.

```php
use edu\wisc\doit\CertificateService;

CertificateService::createPem( 'path/to/certificate', 'path/to/key', 'path/to/save/pem' );
```

### Initializing the client

In addition to the client certificate described above, you will need to provide paths to 2 files:

1. A PEM file that contains the public key of the certificate authority used to sign the webservice endpoint's certificate.
2. Either the URL or the path to a local copy of the WSDL file presented by the target endpoint environment. 

**IMPORTANT:** This particular service is known not to version it's data model and API. It is a regular scenario that the test environment
for this service presents a different WSDL contract than the production environment. 

```php
use edu\wisc\doit\RpcNetidClientSoap;
use edu\wisc\doit\RpcNetidClientSoapConfig;

$config = new RpcNetidClientSoapConfig(
	'/path/to/webservices.endpoint.wsdl',
	'/path/to/webservices.endpoint.certificate.authority.pem',
	'/path/to/pem' );

$client = RpcNetidClientSoap::create( $config );
```

### Sample Call

```php
use edu\wisc\doit\RpcNetidClientSoap;
use edu\wisc\doit\RpcNetidStructGetQuestionsRequest;
use edu\wisc\doit\RpcNetidStructGetQuestionsResponse;

$questions = $client->getQuestions( 'mynetid' );
```

Available calls you can make and their related documentation can be found in the `RpcNetidClient` interface.

## API Documentation

Documentation for the client can be viewed by loading `docs/index.html` into your browser. 
You will need to run `phpdoc` at the root of the project in order to generate them, until they can be published for everyone. 
[Installing phpdoc](http://phpdoc.org/docs/latest/getting-started/installing.html)


## Development Requirements

1. docker [Installation](https://docs.docker.com/install/)
2. docker-compose [Installation](https://docs.docker.com/compose/install/)

## Developer Setup

### Building the Docker Image

If this is your first time running the PHP client or after any changes to the DOCKERFILE, run at the root of the project:

```
docker-compose build
```

## Testing

PHPUnit tests are located in `src/test`. To run the tests, at the root of the project, enter:

```bash
docker-compose run --rm test
```

## Integration Testing

Integration with the MST NetID web service can be tested by running:

```bash
docker-compose run --rm integration-test
```

A valid client certificate is required to authenticate with Middleware's web service. Test data must be supplied in `src/test/resources/integration-test-data.ini`. 
See the class summary in `src/test/integration-tests/RpcNetidClientSoapIT.php` and the comments in `src/test/resources/integration-test-data.SAMPLE.ini`.

